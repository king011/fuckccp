#!/usr/bin/env bash
set -x
Dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
cd "$Dirname" && go build -ldflags "-s -w" && upx watch && \
    tar -zcvf watch.tar.gz watch list.json