package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"sync"
	"time"
)

func main() {
	var (
		help     bool
		filename string
	)
	flag.StringVar(&filename, "conf", "", "configure filename")
	flag.BoolVar(&help, "help", false, "display help")
	flag.Parse()

	if help {
		flag.PrintDefaults()
	} else {
		if filename == "" {
			flag.PrintDefaults()
			os.Exit(1)
		}

		b, e := ioutil.ReadFile(filename)
		if e != nil {
			log.Fatalln(e)
		}
		var cnf []struct {
			ID   string
			Dir  string
			Name string
			Args []string
		}
		e = json.Unmarshal(b, &cnf)
		if e != nil {
			log.Fatalln(e)
		}
		var wait sync.WaitGroup
		for _, item := range cnf {
			if item.Name == "" {
				continue
			}
			wait.Add(1)
			go run(item.ID, item.Dir, item.Name, item.Args...)
		}
		wait.Wait()
	}
}
func run(id, dir, name string, args ...string) {
	for {
		cmd := exec.Command(name, args...)
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout
		if dir != "" {
			cmd.Dir = dir
		}
		e := cmd.Start()
		if e != nil {
			log.Fatalln(e)
		}
		log.Println(id, `is running`)
		e = cmd.Wait()
		log.Println(id, `is exit with`, e)
		time.Sleep(time.Second * 2)
	}
}
