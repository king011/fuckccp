# fuckccp

一些 docker 搭建的編程環境 [https://gitlab.com/king011/fuckccp](https://gitlab.com/king011/fuckccp)

世界上有一個叫做**朝鮮**的邪惡地方 那裏的蓋世太保害怕人民了解真相勝過一切故而封鎖了網路 於是作爲那裏的韭菜想要編程經常無法正確的下載到軟件包

想要獲取到軟件包往往需要設置全局代理 某些狗屎一樣的系統還很難設置(就編程而言狗屎就是指的windows 不要誤會 如果談遊戲那是另外一個平行世界) 就算是linux 設置全局代理雖然容易但副作用很大 比如我正在下載bt 這可能會導致bt通過代理下載 從而導致服務器因爲版權被封

docker 使用 `--cap-add=NET_ADMIN  --cap-add=NET_RAW` 指令則可在獨立的容器中設置iptables 新興的vscode也支持了 ssh 遠程開發 故而想到使用 docker 打包一個帶上代理工具的容器 然後使用 ssh 通過 vim 或 vscode 開發 都是件愜意的事情 於是打包了一些這樣的環境

# tag base

base 容器是所有環境的 基礎 裏面打包了一些常用軟體 和 代理工具(你可以 `FROM king011/fuckccp:base` 來創建自己的環境)

1. openssh-server 提供 sshd 服務 (自動創建了一個 用戶名爲 dev 密碼爲 123 的開發帳號)
2. vim 編輯器
3. git 版本控制
4. curl wget 網頁測試 工具
5. iptables proxychains coredns 代理工具
6. v2ray-web 工作在 18080 端口的 web 圖形接口 提供了 v2ray-core 的功能 和 自動設置 iptables 規則等功能

使用下述指令運行容器
```
docker run --cap-add=NET_ADMIN  --cap-add=NET_RAW \
    -p 127.0.0.1:10022:22/tcp \
    -p 127.0.0.1:18080:18080/tcp \
    --name base \
    --rm \
    -d king011/fuckccp:base
```

使用 `ssh dev@127.0.0.1` 進入容器 開始 vim 編程 或 vscode 遠程編程

# tag go1.6.2

在 base 基礎上安裝了 gcc go1.6.2

```
docker run --cap-add=NET_ADMIN  --cap-add=NET_RAW \
    -p 127.0.0.1:10022:22/tcp \
    -p 127.0.0.1:18080:18080/tcp \
    -v your_home_path:/home/dev \
    -v your_lib_path:/home/lib \
    -v your_project_path:/home/project \
    --name go \
    --rm \
    -d king011/fuckccp:go1.6.2
```

# tag node

* node-v14.16.0 
* yarn
* typescript

```
docker run --cap-add=NET_ADMIN  --cap-add=NET_RAW \
    -p 127.0.0.1:10022:22/tcp \
    -p 127.0.0.1:18080:18080/tcp \
    -v your_lib_path:/home/lib \
    -v your_project_path:/home/project \
    --name node \
    --rm \
    -d king011/fuckccp:node
```

# tag angular11

* FROM tag node
* angular version 11

```
docker run --cap-add=NET_ADMIN  --cap-add=NET_RAW \
    -p 127.0.0.1:10022:22/tcp \
    -p 127.0.0.1:18080:18080/tcp \
    -v your_lib_path:/home/lib \
    -v your_project_path:/home/project \
    --name angular \
    --rm \
    -d king011/fuckccp:angular11
```